<?php

namespace App\Model;

use Eloquent as Model;

/**
 * Class FAQ
 * @package App\Model
 * @version November 11, 2020, 4:58 pm UTC
 *
 * @property string $name
 * @property string $email
 * @property string $message
 */
class FAQ extends Model
{

    public $table = 'faqs';


    public $fillable = [
        'name',
        'email',
        'message',
        'subject',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'      => 'integer',
        'name'    => 'string',
        'email'   => 'string',
        'message' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name'    => 'required',
        'email'   => 'required',
        'subject' => 'required',
        'message' => 'required',
    ];


}
