<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Book;
use App\Model\BookCategory;
use App\Model\Category;
use App\Model\CategoryUnlock;
use App\Model\OldPaper;
use App\Model\PaperCategory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    //unlock category for a user
    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function categoryUnlock(Request $request)
    {
        $data = ['success' => false, 'message' => __('Something Went wrong !')];

        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = [];
            $e = $validator->errors()->all();
            foreach ($e as $error) {
                $errors[] = $error;
            }
            $data['message'] = $errors;

            return response()->json($data);
        }

        try {
            $category = Category::where('id', $request->category_id)->first();
            if (isset($category)) {
                if ($category->coin > 0) {
                    $alreadyUnlock = CategoryUnlock::where([
                        'user_id'     => Auth::user()->id,
                        'category_id' => $request->category_id, 'status' => 0,
                    ])->first();
                    if (isset($alreadyUnlock)) {
                        $data = [
                            'success' => false,
                            'message' => __('This category already unlock'),
                        ];
                    } else {
                        $unlock = CategoryUnlock::create([
                            'user_id'     => Auth::user()->id,
                            'category_id' => $request->category_id, 'status' => 0,
                        ]);
                        if ($unlock) {
                            $data = [
                                'success' => true,
                                'message' => __('Category unlock successfully'),
                            ];
                        } else {
                            $data = [
                                'success' => false,
                                'message' => __('Category unlock failed'),
                            ];
                        }
                    }
                } else {
                    $data = [
                        'success' => false,
                        'message' => __('There is a no coin in this category for unlock'),
                    ];
                }
            } else {
                $data = [
                    'success' => false,
                    'message' => __('Category not found'),
                ];
            }

        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong. Please try again!',
            ]);
        }

        return response()->json($data);
    }

    /**
     * @return JsonResponse
     */
    public function allCategory()
    {
        $data = ['success' => true, 'message' => __('all category retrived Went wrong !')];
        $quizCategories = Category::orderBy('serial', 'ASC')->whereNull('parent_id')->get();
        $data['data']['quizCategories'] = $quizCategories;
        $bookCategories = BookCategory::all();
        $data['data']['bookCategories '] = $bookCategories;
        $paperCategories = PaperCategory::all();
        $data['data']['paperCategories '] = $paperCategories;

        return response()->json($data);
    }

    /**
     * @param $categoryName
     *
     * @return JsonResponse
     */
    public function getCategory($categoryName)
    {
        $data = ['success' => true, 'message' => __('category retrived sucessfully !')];
        if ($categoryName == 'book') {
            $query = Book::with('category');
        }
        if ($categoryName == 'paper') {
            $query = OldPaper::with('category');
        }
        if ($categoryName == 'quiz-category') {
            $query = Category::with('question')->withCount('question');
        }

        if (! isset($query)) {
            $data = ['success' => false, 'message' => __('category not found !')];

            return response()->json($data);
        }
        $data[$categoryName] = $query->get();

        return response()->json($data);
    }

    /**
     * @param $categoryName
     * @param $categoryId
     *
     * @return JsonResponse
     */
    public function getCategoryData($categoryName, $categoryId)
    {
        $data = ['success' => true, 'message' => __('category retrived sucessfully !')];
        if ($categoryName == 'book') {
            $query = Book::find($categoryId);
        }
        if ($categoryName == 'Paper') {
            $query = OldPaper::find($categoryId);
        }
        if ($categoryName == 'quiz-category') {
            $query = Category::find($categoryId);
        }
        if (! isset($query)) {
            $data = ['success' => false, 'message' => __('category not found !')];

            return response()->json($data);
        }
        $data[$categoryName] = $query;

        return response()->json($data);
    }

    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function allBookCategory(Request $request)
    {
        $categoryId = $request->get('id');
        $data = ['success' => true, 'message' => __('category retrived sucessfully !')];
        if ($categoryId) {
            $query = BookCategory::find($categoryId);
        } else {
            $query = BookCategory::all();
        }
        $data['bookCategories'] = $query;

        return response()->json($data);
    }

    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function allPaperCategory(Request $request)
    {
        $categoryId = $request->get('id');
        $data = ['success' => true, 'message' => __('category retrived sucessfully !')];
        if ($categoryId) {
            $query = PaperCategory::find($categoryId);
        } else {
            $query = PaperCategory::all();
        }
        $data['paperCategories'] = $query;

        return response()->json($data);
    }
}
