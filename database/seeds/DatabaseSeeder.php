<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(AdminSettingSeeder::class);
        $this->call(UserCoinSeeder::class);
        $this->call(PaymentMethodSeeder::class);
        $this->call(CoinSeeder::class);
        $this->call(BookCategorySeeder::class);
//        $this->call(BookSeeder::class);
        $this->call(PaperCategorySeeder::class);
//        $this->call(OldPaperSeeder::class);
    }
}
