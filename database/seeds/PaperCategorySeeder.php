<?php

use Illuminate\Database\Seeder;

class PaperCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        foreach (range(1, 20) as $index) {
            $input = [
                'name'   => $faker->name,
                'status' => 1,
            ];

            \App\Model\PaperCategory::create($input);
        }
    }
}
