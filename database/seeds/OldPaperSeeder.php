<?php

use Illuminate\Database\Seeder;

class OldPaperSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $papercategories = \App\Model\PaperCategory::all()->pluck('id')->toArray();


        foreach (range(1, 20) as $index) {
            $input = [
                'name'              => $faker->name,
                'creator_name'      => $faker->name,
                'language'          => 'Hindi',
                'paper_category_id' => array_rand($papercategories),
            ];

            \App\Model\OldPaper::create($input);
        }
    }
}
