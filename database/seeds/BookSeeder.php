<?php

use Illuminate\Database\Seeder;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $bookcategories = \App\Model\BookCategory::all()->pluck('id')->toArray();


        foreach (range(1, 20) as $index) {
            $input = [
                'name'             => $faker->name,
                'seller_name'      => $faker->name,
                'language'         => 'Hindi',
                'book_category_id' => array_rand($bookcategories),
            ];

            \App\Model\Book::create($input);
        }
    }
}
