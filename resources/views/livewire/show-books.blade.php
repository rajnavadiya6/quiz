<div class="row">
    <div class="col-md-12 d-flex justify-content-end">
        <select wire:model="categoryId" class="courses_search_select courses_search_input select2">
            <option value="">All Categories</option>
            @foreach($allbookCategories as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>
            @endforeach
        </select>

    </div>
    <!-- Books Main Content -->
    <div class="col-md-12">
        <div wire:loading>
            <div class="loader"></div>
        </div>
        @foreach($bookCategories as $category)
            <div class="row">
                <div class="col-12">
                    <h4 class="font-weight-bold book_category_name">
                        {{ $category->name }}
                    </h4>
                </div>
                <div class="col-12">
                    <div class="row courses_row">
                        @forelse($category->books as $book)
                            <div class="col-md-3 col-sm-12 course_col">
                                <div class="card course border-0">
                                    <div class="card-img-top mb-2 pt-2 d-flex justify-content-center">
                                        <img src="{{ asset('web_assets/images/marker.png') }}" alt=""></div>
                                    <div class="card-body p-3">
                                        <h3 class="course_title">
                                            {{ $book->name }}
                                        </h3>
                                        <div class="card-text mt-3">
                                            <p>Language: {{ $book->language }} </p>
                                        </div>
                                    </div>
                                    <div class="card-footer bg-white text-center mt-4">
                                        <a target="_blank" href="{{ asset(path_book_pdf().$book->book_pdf) }}"
                                           class="btn btn-outline-primary">View Book</a>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <h4>No Book Found </h4>
                        @endforelse
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="col-md-12 mb-5 d-flex justify-content-center">
        {{ $bookCategories->links() }}
    </div>
</div>
@push('page-js')
    <script>
        document.addEventListener('livewire:load', function () {
            $('.select2').on('change', function (e) {
            @this.set('categoryId', e.target.value);
            });
            window.livewire.hook('afterDomUpdate', () => {
                $('.select2').select2();
            });
        });
    </script>
@endpush
