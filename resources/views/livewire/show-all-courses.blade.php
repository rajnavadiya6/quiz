<div>
    <div class="courses_search_container">
        <form action="#" id="courses_search_form"
              class="courses_search_form d-flex flex-row align-items-center justify-content-start">
            <input type="search" wire:model="search" class="courses_search_input" placeholder="Search Courses"
                   required="required">
            <select wire:model="filter" id="courses_search_select" class=" courses_search_select courses_search_input">
                <option value="Books">Books</option>
                <option value="Papers">Papers</option>
                <option value="QuizCategory">QuizCategory</option>
            </select>
            <button action="submit" class="courses_search_button ml-auto">search now</button>
        </form>
    </div>
    <div class="courses_container">
        <div wire:loading>
            <div class="loader"></div>
        </div>

        <div class="row courses_row custom_scroll">
        @foreach($courses as $course)
            <!-- Course -->
                <div class="col-lg-6 course_col">
                    <div class="course">
                        <div class="course_image">
                            <img src="{{ asset('web_assets/images/course_4.jpg') }}" alt=""></div>
                        <div class="course_body">
                            <h3 class="course_title">
                                @if(class_basename($course) == 'Book')
                                    <a href="#">{{ $course->name }}</a>
                                @endif
                                @if(class_basename($course) == 'Category')
                                    <a href="#">{{ $course->name }}</a>
                                @endif
                            </h3>
                            <div class="course_teacher">
                                @if(class_basename($course) == 'Book')
                                    {{ $course->category ? $course->category->name : '' }}
                                @endif
                                @if(class_basename($course) == 'Category')
                                    {{ $course->description }}
                                @endif
                            </div>
                            <div class="course_text">
                                <p> @if(class_basename($course) == 'Book')
                                        Langauge: {{ $course->language }}
                                    @endif
                                    @if(class_basename($course) == 'Category')
                                        Question Count : {{ $course->question_count }}
                                    @endif
                                </p>
                            </div>
                        </div>
                        <div class="course_footer">
                            <div class="course_footer_content d-flex flex-row align-items-center justify-content-start">
                                <div class="course_info">
                                    <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                                    <span>20 Student</span>
                                </div>
                                <div class="course_info">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <span>5 Ratings</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            @endforeach

        </div>
        <div class="row pagination_row">
            <div class="col">
                <div class="pagination_container d-flex flex-row align-items-center justify-content-start">
                    {{ $courses->links() }}

                    <div class="courses_show_container ml-auto clearfix">
                        <div class="courses_show_text">Showing <span class="courses_showing">1-6</span>
                            of <span class="courses_total">26</span> results:
                        </div>
                        <div class="courses_show_content">
                            <span>Show: </span>
                            <select wire:model="pagination" id="courses_show_select" class="courses_show_select">
                                <option>06</option>
                                <option>12</option>
                                <option>24</option>
                                <option>36</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

