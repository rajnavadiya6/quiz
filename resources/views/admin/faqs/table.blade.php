<table id="category-table" class="table category-table table-bordered  text-center mb-0">
    <thead>
    <tr>
        <th class="all" style="width: 20%;">{{__('SL.')}}</th>
        <th class="desktop">Name</th>
        <th class="desktop">Email</th>
        <th class="desktop">Subject</th>
        <th class="desktop">Message</th>
        <th class="all" style="width: 15%;">{{__('Action')}}</th>
    </tr>
    </thead>
    <tbody>
    @if(isset($faqs[0]))
        @php ($sl = 1)
        @foreach($faqs as $fAQ)
            <tr>
                <td>{{ $sl++ }}</td>
                <td>{{ $fAQ->name }}</td>
                <td>{{ $fAQ->email }}</td>
                <td>{{ $fAQ->subject }}</td>
                <td>{{ $fAQ->message }}</td>
                <td>
                    {!! Form::open(['route' => ['faqs.destroy', $fAQ->id], 'method' => 'delete']) !!}

                    <a href="{{ route('faqs.show', [$fAQ->id]) }}" class="datatabel-links text-info"
                       data-toggle="tooltip" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>
                    <a href="{{ route('faqs.edit', [$fAQ->id]) }}" class="datatabel-links text-warning"
                       data-toggle="tooltip" title="Edit"><i class="fa fa-pencil-square-o"></i></a>
                    {!! Form::button('<i class="fa fa-trash-o"></i>', ['type' => 'submit', 'class' => 'datatabel-links text-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
