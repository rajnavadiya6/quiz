@extends('web.layouts.app')
@push('page-css')
    <link rel="stylesheet" type="text/css" href="{{ asset('web_assets/styles/contact.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('web_assets/styles/contact_responsive.css') }}">
@endpush
@section('main-body')
    @php
        $settings= allsetting();
    @endphp

    @include('web.include.breadcrumbs',['currentName' => 'Contact'])
    <!-- Contact -->

    <div class="contact">

        <!-- Contact Info -->

        <div class="contact_info_container">
            <div class="container">
                @include('layout.message')
                @include('flash::message')
                <div class="row">
                    <!-- Contact Form -->
                    <div class="col-lg-6">
                        <div class="contact_form">
                            <div class="contact_info_title">Contact Form</div>
                            <form action="{{ route('web.contact.store') }}" class="comment_form" method="POST">
                                @csrf
                                <div>
                                    <div class="form_title">Name:</div>
                                    <input type="text" class="comment_input" name="name" required="required">
                                </div>
                                <div>
                                    <div class="form_title">Email:</div>
                                    <input type="email" class="comment_input" name="email" required="required">
                                </div>
                                <input type="hidden" name="subject" value="contact"/>
                                <div>
                                    <div class="form_title">Message:</div>
                                    <textarea class="comment_input comment_textarea" name="message"
                                              required="required"></textarea>
                                </div>
                                <div>
                                    <button type="submit" class="comment_button trans_200">submit now</button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <!-- Contact Info -->
                    <div class="col-lg-6">
                        <div class="contact_info">
                            <div class="contact_info_title">
                                @if($settings['landing_contact_title'])
                                    {{ $settings['landing_contact_title'] }}
                                @else
                                    Contact Info
                                @endif
                            </div>
                            <div class="contact_info_text">
                                <p>@if($settings['landing_contact_des'])
                                        {{ $settings['landing_contact_des'] }}
                                    @else
                                        It is a long established fact that a reader will be distracted by the readable
                                        content of a page when looking at its layout. The point of using Lorem Ipsum is
                                        that
                                        it has a distribution of letters.
                                    @endif
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
