@extends('web.layouts.app')
@section('main-body')
    <!-- Home -->
    <div class="home">
        @include('web.home.banner')
    </div>

    @include('web.home.register_model')
    @include('web.home.login_model')

    <!-- Category -->

    @include('web.home.category_list')

    <!-- Book Category -->

    @include('web.home.book_category_list')

    <!-- Features -->

    @include('web.home.features')


    <!-- Counter -->

    <div class="counter">
        <div class="counter_background"
             style="background-image:url({{ asset('web_assets/images/counter_background.jpg') }})"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="counter_content">
                        <h2 class="counter_title">Register Now</h2>
                        <div class="counter_text">
                            <p>Simply dummy text of the printing and typesetting industry. Lorem
                                Ipsum has been the industry’s standard dumy text ever since the 1500s, when an unknown
                                printer took a galley of type and scrambled it to make a type specimen book.</p>
                        </div>

                        <!-- Milestones -->

                        <div class="milestones d-flex flex-md-row flex-column align-items-center justify-content-between">

                            <!-- Milestone -->
                            <div class="milestone">
                                <div class="milestone_counter" data-end-value="15">0</div>
                                <div class="milestone_text">years</div>
                            </div>

                            <!-- Milestone -->
                            <div class="milestone">
                                <div class="milestone_counter" data-end-value="120" data-sign-after="k">0</div>
                                <div class="milestone_text">years</div>
                            </div>

                            <!-- Milestone -->
                            <div class="milestone">
                                <div class="milestone_counter" data-end-value="670" data-sign-after="+">0</div>
                                <div class="milestone_text">years</div>
                            </div>

                            <!-- Milestone -->
                            <div class="milestone">
                                <div class="milestone_counter" data-end-value="320">0</div>
                                <div class="milestone_text">years</div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>

            <div class="counter_form">
                <div class="row fill_height">
                    <div class="col fill_height">
                        <form method="POST"
                              class="counter_form_content d-flex flex-column align-items-center justify-content-center"
                              action="{{ route('web.contact.store')  }}">
                            @csrf
                            <div class="counter_form_title">courses now</div>
                            <input type="text" class="counter_input" name="name" placeholder="Your Name:"
                                   required="required">
                            <input type="email" class="counter_input " name="email" placeholder="email:"
                                   required="required">
                            <select name="subject" id="counter_select" class="counter_input counter_options" required>
                                <option value="">Choose Subject</option>
                                <option>Query</option>
                                <option>Contact</option>
                                <option>Other</option>
                            </select>
                            <textarea class="counter_input counter_text_input" name="message" placeholder="Message:"
                                      required="required"></textarea>
                            <button type="submit" class="counter_form_button">submit now</button>
                        </form>
                        <div class="my-2">
                            @include('layout.message')
                            @include('flash::message')
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

    <!-- Latest News -->

    <div class="news">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="section_title_container text-center">
                        <h2 class="section_title">Latest News</h2>
                        <div class="section_subtitle">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec
                                vel gravida arcu. Vestibulum feugiat, sapien ultrices fermentum congue, quam velit
                                venenatis sem</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row news_row">
                <div class="col-lg-7 news_col">

                    <!-- News Post Large -->
                    <div class="news_post_large_container">
                        <div class="news_post_large">
                            <div class="news_post_image"><img src="{{ asset('web_assets/images/news_1.jpg') }}" alt="">
                            </div>
                            <div class="news_post_large_title"><a href="blog_single.html">Here’s What You Need to Know
                                    About Online Testing for the ACT and SAT</a></div>
                            <div class="news_post_meta">
                                <ul>
                                    <li><a href="#">admin</a></li>
                                    <li><a href="#">november 11, 2017</a></li>
                                </ul>
                            </div>
                            <div class="news_post_text">
                                <p>Policy analysts generally agree on a need for reform, but not on which path
                                    policymakers should take. Can America learn anything from other nations...</p>
                            </div>
                            <div class="news_post_link"><a href="blog_single.html">read more</a></div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-5 news_col">
                    <div class="news_posts_small">

                        <!-- News Posts Small -->
                        <div class="news_post_small">
                            <div class="news_post_small_title"><a href="blog_single.html">Home-based business insurance
                                    issue (Spring 2017 - 2018)</a></div>
                            <div class="news_post_meta">
                                <ul>
                                    <li><a href="#">admin</a></li>
                                    <li><a href="#">november 11, 2017</a></li>
                                </ul>
                            </div>
                        </div>

                        <!-- News Posts Small -->
                        <div class="news_post_small">
                            <div class="news_post_small_title"><a href="blog_single.html">2018 Fall Issue: Credit Card
                                    Comparison Site Survey (Summer 2018)</a></div>
                            <div class="news_post_meta">
                                <ul>
                                    <li><a href="#">admin</a></li>
                                    <li><a href="#">november 11, 2017</a></li>
                                </ul>
                            </div>
                        </div>

                        <!-- News Posts Small -->
                        <div class="news_post_small">
                            <div class="news_post_small_title"><a href="blog_single.html">Cuentas de cheques gratuitas
                                    una encuesta de Consumer Action</a></div>
                            <div class="news_post_meta">
                                <ul>
                                    <li><a href="#">admin</a></li>
                                    <li><a href="#">november 11, 2017</a></li>
                                </ul>
                            </div>
                        </div>

                        <!-- News Posts Small -->
                        <div class="news_post_small">
                            <div class="news_post_small_title"><a href="blog_single.html">Troubled borrowers have fewer
                                    repayment or forgiveness options</a></div>
                            <div class="news_post_meta">
                                <ul>
                                    <li><a href="#">admin</a></li>
                                    <li><a href="#">november 11, 2017</a></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
