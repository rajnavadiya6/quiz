<!-- Modal -->
<div id="loginModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title text-center">Login</h3>
                <button type="button" class="close" data-dismiss="modal">&times;
                </button>
            </div>
            <div class="modal-body">
                <div class="col fill_height">
                    <form class="counter_form_content d-flex flex-column align-items-center justify-content-center"
                          action="{{ route('loginProcess') }}" method="POST">
                        @csrf
                        <div class="counter_form_title">Login</div>

                        <input type="email" name="email" class="counter_input"
                               placeholder="Enter email" required>
                        <input type="password" name="password" class="counter_input"
                               placeholder="Password" required>
                        <button type="submit" class="counter_form_button">
                            Login
                        </button>
                        <div class="qz-user-footer  text-center">
                            <span class="text-left">
                                @include('layout.message')
                            </span>
                            <div class="mt-3">
                                <a href="{{route('forgetPassword')}}">
                                    forgot password ?</a>
                            </div>
                            <div><a href="{{route('privacyPolicy')}}">
                                    privacy policy</a> and
                                <a href="{{route('termsCondition')}}">
                                    terms&amp; conditions</a></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
