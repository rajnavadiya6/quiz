<!-- Header -->

<header class="header @auth scrolled_login @endauth ">

    <!-- Top Bar -->
    @guest
        <div class="top_bar">
            <div class="top_bar_container">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="top_bar_content d-flex flex-row align-items-center justify-content-start">
                                <ul class="top_bar_contact_list">
                                    <li>
                                        <div class="question">Have any questions?</div>
                                    </li>
                                    <li>
                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                        <div>001-1234-88888</div>
                                    </li>
                                    <li>
                                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                        <div>info.deercreative@gmail.com</div>
                                    </li>
                                </ul>
                                <div class="top_bar_login ml-auto">
                                    <div class="login_button"><a href="#">Register or Login</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endguest
<!-- Header Content -->
    <div class="header_container">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="header_content d-flex flex-row align-items-center justify-content-start">
                        <div class="logo_container">
                            <a href="{{ route('web.home') }}">
                                <div class="logo_text">
                                    @if(getAppName())
                                        {!!  substr(getAppName(),0,-2) . '<span>'. substr(getAppName(),-2,2).'</span>' !!}

                                    @else
                                        Qu<span>iz</span>
                                    @endif
                                </div>
                            </a>
                        </div>
                        <nav class="main_nav_contaner ml-auto">
                            <ul class="main_nav">
                                <li class="{{ (url()->current() == route('web.home')) ? 'active' : '' }}">
                                    <a href="{{ route('web.home') }}">
                                        Home
                                    </a>
                                </li>

                                {{--                                <li class=" {{ (url()->current() == route('web.about')) ? 'active' : '' }}">--}}
                                {{--                                    <a href="{{ route('web.about') }}">--}}
                                {{--                                        About--}}
                                {{--                                    </a>--}}
                                {{--                                </li>--}}

                                <li class=" {{ Request::is('courses*') ? 'active' : '' }}">
                                    <a href="{{ route('web.courses') }}">
                                        Courses
                                    </a>
                                </li>

                                {{--                                <li class=" {{ (url()->current() == route('web.blog')) ? 'active' : '' }}">--}}
                                {{--                                    <a href="{{ route('web.blog') }}">--}}
                                {{--                                        Blog--}}
                                {{--                                    </a>--}}
                                {{--                                </li>--}}

                                <li class=" {{ (url()->current() == route('web.contact')) ? 'active' : '' }}">
                                    <a href="{{ route('web.contact') }}">
                                        Contact
                                    </a>
                                </li>
                                @auth
                                    <li>
                                        <a href="{{ (Auth::user()->role == 1) ? route('adminDashboardView') : route('userDashboardView') }}">
                                            Dashbord
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('logOut')}}">
                                            {{__('Logout')}}
                                        </a>
                                    </li>
                                @endauth

                            </ul>
                            <!-- Hamburger -->
                            <div class="hamburger menu_mm">
                                <i class="fa fa-bars menu_mm" aria-hidden="true"></i>
                            </div>
                        </nav>

                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Menu -->

<div class="menu d-flex flex-column align-items-end justify-content-start text-right menu_mm trans_400">
    <div class="menu_close_container">
        <div class="menu_close">
            <div></div>
            <div></div>
        </div>
    </div>
    <div class="search">
        <form action="#" class="header_search_form menu_mm">
            <input type="search" class="search_input menu_mm" placeholder="Search" required="required">
            <button class="header_search_button d-flex flex-column align-items-center justify-content-center menu_mm">
                <i class="fa fa-search menu_mm" aria-hidden="true"></i>
            </button>
        </form>
    </div>
    <nav class="menu_nav">
        <ul class="menu_mm">
            <li class="menu_mm"><a href="index.html">Home</a></li>
            <li class="menu_mm"><a href="#">About</a></li>
            <li class="menu_mm"><a href="#">Courses</a></li>
            <li class="menu_mm"><a href="#">Blog</a></li>
            <li class="menu_mm"><a href="#">Page</a></li>
            <li class="menu_mm"><a href="contact.html">Contact</a></li>
        </ul>
    </nav>
</div>

<!-- Home -->
