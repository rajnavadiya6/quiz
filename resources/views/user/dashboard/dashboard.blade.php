@extends('user.master')
@section('title') @if (isset($pageTitle)) {{ $pageTitle }} @endif @endsection

@section('main-body')
    <!-- category-area start-->
    <div class="category-area">
        <div class="category-wrap m-5">
            @include('layout.message')
            <div class="row">
                <div class="col-lg-3 border-right">
                    @include('user.dashboard.user_side_menu')
                </div>
                <div class="col-lg-8">
                    <div class="tab-content">
                        <div id="Science" class="tab-pane active">
                            <div class="category-sub-area category-sub-area-p">
                                @if(isset($categories[0]))
                                    <div class="row">
                                        @foreach($categories as $item)
                                            @include('user.category.category_list')
                                        @endforeach
                                    </div>
                                @else
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <p class="text-danger text-center">
                                                {{__('No data found')}}</p>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- category-area end-->
    @include('user.category.unlock_category_model')

@endsection

@section('script')
    <script>
        function open_modal (id, coin, name) {
            $('#UnlockModal').modal('show');
            $('.cat-name').text(name);
            $('#cat-coin').text(coin);
            $('#cat-id').val(id);
        };

        function myarray (id) {
            console.log(id);
            alert(id);
        }

        var colors = ['#ff0000', '#00ff00', '#0000ff'];
        var random_color = colors[Math.floor(Math.random() * colors.length)];
        // document.getElementById('title').style.color = random_color;
    </script>
@endsection
