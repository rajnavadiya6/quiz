<!-- Icon fonts -->
<link href="{{asset('assets/user/css/font-awesome.min.css')}}" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
      integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA=="
      crossorigin="anonymous"/>
<link href="{{asset('assets/user/css/themify-icons.css')}}" rel="stylesheet">
<!-- Bootstrap core CSS -->
{{--<link href="{{asset('assets/user/css/bootstrap.min.css')}}" rel="stylesheet">--}}
<link rel="stylesheet" type="text/css" href="{{ asset('web_assets/styles/bootstrap4/bootstrap.min.css') }}">

<!-- Plugins for this template -->
<link href="{{asset('assets/user/css/jquery-ui.css')}}" rel="stylesheet">
<link href="{{asset('assets/user/css/animate.css')}}" rel="stylesheet">
<link href="{{asset('assets/user/css/owl.carousel.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/user/css/jquery.toast.css')}}" rel="stylesheet">
<link href="{{asset('assets/user/css/slicknav.min.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('assets/DataTables/css/jquery.dataTables.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/DataTables/css/responsive.dataTables.min.css')}}">
<!--for image drag and drop-->
<link rel="stylesheet" type="text/css" href="{{asset('assets/dropify/css/dropify.min.css')}}">
<!-- Custom styles for this template -->
<link href="{{asset('assets/user/css/style.css')}}" rel="stylesheet">
<link href="{{asset('assets/user/css/responsive.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('web_assets/styles/main_styles.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('web_assets/plugins/OwlCarousel2-2.2.1/animate.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/components.css')}}">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
